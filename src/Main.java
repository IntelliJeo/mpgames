import java.util.*;
import java.util.Random;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.*;



public class Main {
    private static final String SAVE_FILE_NAME = "save.txt";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to the Game Console!");
        System.out.println("Choose a game to play:");
        System.out.println("1. Snakes and Ladders");
        System.out.println("2. Tic Tac Toe");
        System.out.println("3. ROCK SCISSORS PAPER");
        System.out.println("4. EXIT!");

        int gameChoice = scanner.nextInt();
        if (gameChoice == 1) {
            playSnakesAndLadders(scanner);
        } else if (gameChoice == 2) {
            TicTacToeGame.main();
        } else if (gameChoice == 3) {
            RockScissorsPaper.main();
        }
        else if (gameChoice == 4) {
            return;
        } else {
            System.out.println("Invalid choice. Exiting...");
        }
    }

    public static void playSnakesAndLadders(Scanner scanner) {
        System.out.println("Welcome to Snakes and Ladders!\n");
        System.out.println("***Snakes and Ladders is a classic board game where players take turns rolling a die and moving their token on the board,\n trying to reach the end before their opponents.***\n");

        System.out.println("How many players are there?");
        int numPlayers = scanner.nextInt();
        scanner.nextLine();

        String[] playerNames = new String[numPlayers];
        int[] playerPositions = new int[numPlayers];

        //random na arxizei
        Random random = new Random();
        int currentPlayer = random.nextInt(numPlayers);


        // Initialize player positions to 1
        Arrays.fill(playerPositions, 1);

        System.out.println("Do you want to start a new game or load a saved game?");
        System.out.println("1. New game");
        System.out.println("2. Load game");
        int choice = scanner.nextInt();
        scanner.nextLine();

        if (choice == 2) {
            if (loadGame(playerNames, playerPositions, new int[]{currentPlayer})) {
                System.out.println("Game loaded successfully.");
            } else {
                System.out.println("Failed to load the game. Starting a new game...");
            }
        } else {
            for (int i = 0; i < numPlayers; i++) {
                System.out.print("Enter name for Player " + (i + 1) + ": ");
                playerNames[i] = scanner.nextLine();
            }
        }

        System.out.println("\n**Type 'save' anytime to save the game.**");

        while (true) {
            System.out.println("\n" + playerNames[currentPlayer] + ", it's your turn. Press Enter to roll the dice.");
            scanner.nextLine();

            int diceRoll = (int) (Math.random() * 6) + 1;
            System.out.println("You rolled a " + diceRoll);

            playerPositions[currentPlayer] += diceRoll;
            if (playerPositions[currentPlayer] > 100) {
                playerPositions[currentPlayer] = 100 - (playerPositions[currentPlayer] - 100);
            }

            int newPosition = getNewPosition(playerPositions[currentPlayer]);
            if (newPosition != playerPositions[currentPlayer]) {
                if (newPosition > playerPositions[currentPlayer]) {
                    System.out.println("You landed on a ladder! Climb up to position " + newPosition);
                } else {
                    System.out.println("You landed on a snake! Slide down to position " + newPosition);
                }
            }
            playerPositions[currentPlayer] = newPosition;

            System.out.println(playerNames[currentPlayer] + " is now at position " + playerPositions[currentPlayer]);

            if (playerPositions[currentPlayer] == 100) {
                System.out.println(playerNames[currentPlayer] + " wins!");
                break;
            }

            // Check if the current player landed on special positions 25, 50, 75
            if (playerPositions[currentPlayer] == 25 || playerPositions[currentPlayer] == 50 || playerPositions[currentPlayer] == 75) {
                System.out.println("You landed on a special square! You get to play again.");
                continue;
            }

            // Check if the current player landed on positions 30, 60, or 90
            if (playerPositions[currentPlayer] == 30 || playerPositions[currentPlayer] == 60 || playerPositions[currentPlayer] == 90) {
                System.out.println("You landed on a black square! The next player will play twice.");
                currentPlayer = (currentPlayer + 1) % numPlayers;
                int nextPlayer = (currentPlayer + 1) % numPlayers;
                System.out.println(playerNames[currentPlayer] + ", it's your turn to play twice. Press Enter to roll the dice.");
                scanner.nextLine();

                int firstRoll = (int) (Math.random() * 6) + 1;
                int secondRoll = (int) (Math.random() * 6) + 1;
                int totalRoll = firstRoll + secondRoll;

                System.out.println("You rolled a " + firstRoll + " and a " + secondRoll + ", for a total of " + totalRoll);

                playerPositions[currentPlayer] += totalRoll;
                if (playerPositions[currentPlayer] > 100) {
                    playerPositions[currentPlayer] = 100 - (playerPositions[currentPlayer] - 100);
                }
                newPosition = getNewPosition(playerPositions[currentPlayer]);
                if (newPosition != playerPositions[currentPlayer]) {
                    if (newPosition > playerPositions[currentPlayer]) {
                        System.out.println("You landed on a ladder! Climb up to position " + newPosition);
                    } else {
                        System.out.println("You landed on a snake! Slide down to position " + newPosition);
                    }
                }
                playerPositions[currentPlayer] = newPosition;

                System.out.println(playerNames[currentPlayer] + " is now at position " + playerPositions[currentPlayer]);

                if (playerPositions[currentPlayer] == 100) {
                    System.out.println(playerNames[currentPlayer] + " wins!");
                    break;
                }

                currentPlayer = (currentPlayer + 1) % numPlayers;
                continue;
            }

            currentPlayer = (currentPlayer + 1) % numPlayers;

            if (scanner.hasNextLine()) {
                String input = scanner.nextLine();
                if (input.equalsIgnoreCase("save")) {
                    if (saveGame(playerNames, playerPositions, currentPlayer)) {
                        System.out.println("Game saved successfully.");
                    } else {
                        System.out.println("Failed to save the game.");
                    }
                }
            }
        }


        while (true) {
            System.out.println("\nDo you want to:");
            System.out.println("1. Play again");
            System.out.println("2. Go back to the console menu");
            System.out.println("3. Exit");

            int playAgainChoice = scanner.nextInt();
            scanner.nextLine();

            if (playAgainChoice == 1) {
                resetGame(playerPositions);
                playSnakesAndLadders(scanner);
                break;
            } else if (playAgainChoice == 2) {
                main(new String[0]);
                break;
            } else if (playAgainChoice == 3) {
                break;
            } else {
                System.out.println("Invalid choice. Please choose again.");
            }
        }
    }

    public static boolean saveGame(String[] playerNames, int[] playerPositions, int currentPlayer) {
        try {
            PrintWriter writer = new PrintWriter(new FileWriter(SAVE_FILE_NAME));
            writer.println(playerNames.length);
            for (String name : playerNames) {
                writer.println(name);
            }
            for (int position : playerPositions) {
                writer.println(position);
            }
            writer.println(currentPlayer);
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean loadGame(String[] playerNames, int[] playerPositions, int[] currentPlayer) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(SAVE_FILE_NAME));
            int numPlayers = Integer.parseInt(reader.readLine());
            for (int i = 0; i < numPlayers; i++) {
                playerNames[i] = reader.readLine();
            }
            for (int i = 0; i < numPlayers; i++) {
                playerPositions[i] = Integer.parseInt(reader.readLine());
            }
            currentPlayer[0] = Integer.parseInt(reader.readLine());
            reader.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void resetGame(int[] playerPositions) {
        Arrays.fill(playerPositions, 1);
    }



    public static int getNewPosition(int currentPosition) {
        switch (currentPosition) {
            case 4:
                return 14;
            case 9:
                return 31;
            case 17:
                return 7;
            case 20:
                return 38;
            case 28:
                return 84;
            case 40:
                return 59;
            case 51:
                return 67;
            case 54:
                return 34;
            case 62:
                return 19;
            case 63:
                return 81;
            case 64:
                return 60;
            case 71:
                return 91;
            case 87:
                return 24;
            case 93:
                return 73;
            case 95:
                return 75;
            case 99:
                return 78;
            default:
                return currentPosition;


        }


    }



    //kremala

    public static class TicTacToeGame {
        public static void main() {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Welcome to Tic Tac Toe!\n");
            System.out.println("Tic Tac Toe is a two-player game played on a 3x3 grid. The players take turns placing their mark (usually X or O) in one of the empty squares on the board. The first player to get three of their marks in a row (horizontally, vertically, or diagonally) wins the game. If all of the squares are filled and no player has won, the game ends in a draw.\n");
            System.out.println("Choose a mode:");
            System.out.println("1.PVP");
            System.out.println("2.vs Computer");
            //epilogh mode tou paixnidiou
            int gameChoice = scanner.nextInt();
            if (gameChoice == 1) {
                TicTacToe.main();
            } else if (gameChoice == 2) {
                TicTacToe.TicTacToeCPU.main();
            }
            //code gia to exit
            else {
                System.out.println("Invalid choice. Exiting...");
                System.exit(0);
            }
        }





        public class TicTacToe {
            public static void main() {
                Scanner scanner = new Scanner(System.in);

                System.out.println("Welcome to Tic Tac Toe PvP\n");

                System.out.print("Enter player 1's name: ");
                String player1Name = scanner.nextLine();

                System.out.print("Enter player 2's name: ");
                String player2Name = scanner.nextLine();

                boolean exitGame = false;
                while (!exitGame) {
                    System.out.print("\nDo you want to play a single game or a series of 3 games? (Enter 1 for single game, 3 for series): ");
                    int numGames = scanner.nextInt();

                    int player1Wins = 0;
                    int player2Wins = 0;

// protos paixths na bgainei random
                    
                    boolean player1First = Math.random() < 0.5;

                    for (int i = 0; i < numGames; i++) {
                        System.out.println("\nStarting game " + (i + 1));

                        char[][] board = {
                                {' ', ' ', ' '},
                                {' ', ' ', ' '},
                                {' ', ' ', ' '}
                        };

                        printBoard(board);

                        boolean player1Turn = player1First;
                        boolean gameFinished = false;

                        while (!gameFinished) {
                            if (player1Turn) {
                                playerTurn(board, scanner, 'X', player1Name);
                            } else {
                                playerTurn(board, scanner, 'O', player2Name);
                            }

                            if (hasContestantWon(board, 'X')) {
                                printBoard(board);
                                System.out.println(player1Name + " WINS!");
                                player1Wins++;
                                gameFinished = true;
                            } else if (hasContestantWon(board, 'O')) {
                                printBoard(board);
                                System.out.println(player2Name + " WINS!");
                                player2Wins++;
                                gameFinished = true;
                            } else if (isGameDraw(board)) {
                                printBoard(board);
                                System.out.println("DRAW!");
                                gameFinished = true;
                            }  //o nikitis na paizei protos
                            else {
                                player1Turn = !player1Turn;
                                printBoard(board);
                            }
                        }

                        player1First = (i < numGames - 1) ? player1Turn : (Math.random() < 0.5);
                    }

                    System.out.println("\nGAME OVER");

                    if (player1Wins > player2Wins) {
                        System.out.println(player1Name + " wins the series " + player1Wins + " to " + player2Wins);
                    } else if (player2Wins > player1Wins) {
                        System.out.println(player2Name + " wins the series " + player2Wins + " to " + player1Wins);
                    } else {
                        System.out.println("The series is tied " + player1Wins + " to " + player2Wins);
                    }

                    System.out.print("\nDo you want to play again (1), or exit the game (2)? ");
                    int choice = scanner.nextInt();

                    switch (choice) {
                        case 1:
                            break;

                        case 2:
                            exitGame = true;
                            break;
                        default:
                            System.out.println("Invalid choice, exiting app.");
                            exitGame = true;
                            break;
                    }
                }

                scanner.close();
            }


            private static boolean isGameDraw(char[][] board) {
                for (int i = 0; i < board.length; i++) {
                    for (int j = 0; j < board[i].length; j++) {
                        if (board[i][j] == ' ') {
                            return false; // there are still empty cells, game is not a draw
                        }
                    }
                }
                // all cells are filled and no winner has been determined, game is a draw
                printBoard(board);
                System.out.println("ISOPALIA!");
                return true;
            }


            private static void playerTurn(char[][] board, Scanner scanner, char symbol, String playerName) {
                boolean validMove = false;
                long startTime = System.currentTimeMillis(); // Record the start time

                while (!validMove) {
                    System.out.print(playerName + " (" + symbol + ") enter move (1-9): ");
                    int move = scanner.nextInt();

                    long currentTime = System.currentTimeMillis(); // Get the current time

                    // Check if the player has exceeded the time limit (10 seconds)
                    if (currentTime - startTime > 10000) {
                        System.out.println("Time's up! You have exceeded the time limit. Next player's turn.");
                        return; // Return without making a move, the next player will play
                    }

                    int row = (move - 1) / 3;
                    int col = (move - 1) % 3;

                    if (row < 0 || row > 2 || col < 0 || col > 2) {
                        System.out.println("Invalid move, please enter a number between 1 and 9.");
                        continue;
                    }

                    if (board[row][col] != ' ') {
                        System.out.println("That spot is already taken, please choose another.");
                        continue;
                    }

                    board[row][col] = symbol;
                    validMove = true;
                }
            }








            private static boolean hasContestantWon(char[][] board, char symbol) {
                if ((board[0][0] == symbol && board[0][1] == symbol && board[0][2] == symbol) ||
                        (board[1][0] == symbol && board[1][1] == symbol && board[1][2] == symbol) ||
                        (board[2][0] == symbol && board[2][1] == symbol && board[2][2] == symbol) ||

                        (board[0][0] == symbol && board[1][0] == symbol && board[2][0] == symbol) ||
                        (board[0][1] == symbol && board[1][1] == symbol && board[2][1] == symbol) ||
                        (board[0][2] == symbol && board[1][2] == symbol && board[2][2] == symbol) ||

                        (board[0][0] == symbol && board[1][1] == symbol && board[2][2] == symbol) ||
                        (board[0][2] == symbol && board[1][1] == symbol && board[2][0] == symbol)) {
                    return true;
                }
                return false;
            }





            private static void printBoard(char[][] board) {
                System.out.println(board[0][0] + "|" + board[0][1] + "|" + board[0][2]);
                System.out.println("-----");
                System.out.println(board[1][0] + "|" + board[1][1] + "|" + board[1][2]);
                System.out.println("-----");
                System.out.println(board[2][0] + "|" + board[2][1] + "|" + board[2][2]);
            }

            public static class TicTacToeCPU {
                static ArrayList<Integer> playerPositions = new ArrayList<Integer>();
                static ArrayList<Integer> cpuPositions = new ArrayList<Integer>();

                public static void main() {
                    char[][] gameBoard = {
                            {' ', '|', ' ', '|', ' '},
                            {'-', '+', '-', '+', '-'},
                            {' ', '|', ' ', '|', ' '},
                            {'-', '+', '-', '+', '-'},
                            {' ', '|', ' ', '|', ' '}
                    };
                    printGameBoard(gameBoard);

                    while (true) {
                        Scanner scan = new Scanner(System.in);
                        System.out.println("Enter your placement (1-9):");
                        int playerPos = scan.nextInt();
                        while (playerPositions.contains(playerPos) || cpuPositions.contains(playerPos)) {
                            System.out.println("position taken! Enter a correct Position");
                            playerPos = scan.nextInt();
                        }

                        placePiece(gameBoard, playerPos, "player");
                        String result = checkWinner();
                        if (result.length() > 0) {
                            System.out.println(result);
                            break;
                        }
                        Random rand = new Random();
                        int cpuPos = rand.nextInt(9) + 1;
                        while (playerPositions.contains(cpuPos) || cpuPositions.contains(cpuPos)) {
                            cpuPos = rand.nextInt(9) + 1;
                        }
                        placePiece(gameBoard, cpuPos, "cpu");
                        printGameBoard(gameBoard);
                        result = checkWinner();
                        if (result.length() > 0) {
                            System.out.println(result);
                            break;
                        }

                    }

                }

                public static void printGameBoard(char[][] gameBoard) {
                    for (char[] row : gameBoard) {
                        for (char c : row) {
                            System.out.print(c);
                        }
                        System.out.println();
                    }
                }

                public static void placePiece(char[][] gameBoard, int pos, String user) {

                    char symbol = ' ';

                    if (user.equals("player")) {
                        symbol = 'X';
                        playerPositions.add(pos);
                    } else if (user.equals("cpu")) {
                        symbol = 'O';
                        cpuPositions.add(pos);
                    }

                    switch (pos) {
                        case 1:
                            gameBoard[0][0] = symbol;
                            break;
                        case 2:
                            gameBoard[0][2] = symbol;
                            break;
                        case 3:
                            gameBoard[0][4] = symbol;
                            break;
                        case 4:
                            gameBoard[2][0] = symbol;
                            break;
                        case 5:
                            gameBoard[2][2] = symbol;
                            break;
                        case 6:
                            gameBoard[2][4] = symbol;
                            break;
                        case 7:
                            gameBoard[4][0] = symbol;
                            break;
                        case 8:
                            gameBoard[4][2] = symbol;
                            break;
                        case 9:
                            gameBoard[4][4] = symbol;
                            break;
                        default:
                            break;
                    }
                }

                public static String checkWinner() {

                    List topRow = Arrays.asList(1, 2, 3);
                    List midRow = Arrays.asList(4, 5, 6);
                    List botRow = Arrays.asList(7, 8, 9);
                    List leftCol = Arrays.asList(1, 2, 7);
                    List midCol = Arrays.asList(2, 5, 8);
                    List rightCol = Arrays.asList(3, 6, 9);
                    List cross1 = Arrays.asList(1, 5, 9);
                    List cross2 = Arrays.asList(3, 5, 7);

                    List<List> winConditions = new ArrayList<List>();
                    winConditions.add(topRow);
                    winConditions.add(midRow);
                    winConditions.add(botRow);
                    winConditions.add(leftCol);
                    winConditions.add(midCol);
                    winConditions.add(rightCol);
                    winConditions.add(cross1);
                    winConditions.add(cross2);

                    for (List l : winConditions) {
                        if (playerPositions.containsAll(l)) {
                            return "Congratulations you won!";
                        } else if (cpuPositions.containsAll(l)) {
                            return "CPU wins! Sorry :-(";
                        } else if (playerPositions.size() + cpuPositions.size() == 9) {
                            return "CAT";
                        }
                    }

                    return "";
                }


            }
        }
    }
    public static class RockScissorsPaper {
        public static void main() {
            Scanner sc = new Scanner(System.in);
            Random rand = new Random();
            int num;
            String userChoice = "";
            String computerChoice = "";
            int userWins = 0;
            int computerWins = 0;

            System.out.println("Welcome to Rock, Paper, and Scissors!");
            System.out.println("Rock paper scissors is an intransitive hand game, usually played between two people, in which each player simultaneously forms one of three shapes with an outstretched hand. These shapes are \"rock\", \"paper\", and \"scissors\"");


            // Repeat this section
            for (int i = 0; i < 7; i++) {
                System.out.print("\nPlease choose R)ock, P)aper, or S)cissors. > ");
                try {
                    Thread.sleep(3000); // delay for 3 seconds (3000 milliseconds)
                    userChoice = sc.nextLine();

                    // Computer choice
                    num = rand.nextInt(3);

                    if (num == 0) {
                        computerChoice = "R";
                    } else if (num == 1) {
                        computerChoice = "P";
                    } else if (num == 2) {
                        computerChoice = "S";
                    }

                    // Print the computer choice
                    if (computerChoice.equals("S")) {
                        System.out.println("The computer chose scissors.");
                    } else if (computerChoice.equals("R")) {
                        System.out.println("The computer chose rock.");
                    } else if (computerChoice.equals("P")) {
                        System.out.println("The computer chose paper.");
                    }

                    // Determine a winner
                    if (userChoice.equals("R") && computerChoice.equals("S")) {
                        System.out.println("The user won!");
                        userWins++;
                    } else if (userChoice.equals("P") && computerChoice.equals("R")) {
                        System.out.println("The user won!");
                        userWins++;
                    } else if (userChoice.equals("S") && computerChoice.equals("P")) {
                        System.out.println("The user won!");
                        userWins++;
                    } else if (userChoice.equals("S") && computerChoice.equals("R")) {
                        System.out.println("The computer won!");
                        computerWins++;
                    } else if (userChoice.equals("R") && computerChoice.equals("P")) {
                        System.out.println("The computer won!");
                        computerWins++;
                    } else if (userChoice.equals("P") && computerChoice.equals("S")) {
                        System.out.println("The computer won!");
                        computerWins++;
                    } else if (userChoice.equals(computerChoice)) {
                        System.out.println("Tie!");
                    }
                    // End repeat here
                    System.out.println("User Wins: " + userWins + " Computer Wins: " + computerWins);
                }catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }


            // Determine the final winner
            if (userWins > computerWins) {
                System.out.println("The user is the ultimate winner!");
            } else if (userWins < computerWins) {
                System.out.println("The computer is the ultimate winner!");
            } else {
                System.out.println("The game is tied!");
            }
        }
    }
}