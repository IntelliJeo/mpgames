package src;
import java.util.Scanner;

public class TicTacToe {
    static Scanner scanner = new Scanner(System.in);
    static char[][] board = new char[3][3];
    static char currentPlayerMark;

    public static void main(String[] args) {
        System.out.println("Welcome to Tic Tac Toe!");
        System.out.println("Please enter the name of Player 1: ");
        String player1 = scanner.nextLine();
        System.out.println("Please enter the name of Player 2: ");
        String player2 = scanner.nextLine();
        initializeBoard();
        printBoard();

        String currentPlayer = player1;
        while (!isBoardFull()) {
            System.out.println(currentPlayer + ", please enter the row and column to place your mark (e.g. 1 1 for the top-left corner): ");
            int row = scanner.nextInt() - 1;
            int col = scanner.nextInt() - 1;

            if (isValidMove(row, col)) {
                placeMark(row, col);
                printBoard();
                if (checkForWin()) {
                    System.out.println(currentPlayer + " wins!");
                    break;
                }
                currentPlayer = (currentPlayer == player1) ? player2 : player1;
            } else {
                System.out.println("Invalid move. Please try again.");
            }
        }

        if (!checkForWin()) {
            System.out.println("Tie game!");
        }

        System.out.println("Thanks for playing! Would you like to play again? (y/n)");
        String playAgain = scanner.next();
        if (playAgain.equalsIgnoreCase("y")) {
            main(args);
        } else {
            System.exit(0);
        }
    }

    public static boolean isBoardFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public static void initializeBoard() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = '-';
            }
        }
    }

    public static void printBoard() {
        System.out.println("-------------");
        for (int row = 0; row < 3; row++) {
            System.out.print("| ");
            for (int col = 0; col < 3; col++) {
                System.out.print(board[row][col] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }

    public static boolean isValidMove(int row, int col) {
        if (row < 0 || row > 2 || col < 0 || col > 2) {
            return false;
        } else if (board[row][col] != '-') {
            return false;
        } else {
            return true;
        }
    }

    public static void placeMark(int row, int col) {
        board[row][col] = currentPlayerMark;
        currentPlayerMark = (currentPlayerMark == 'X') ? 'O' : 'X';
    }

    public static boolean checkForWin() {
        for (int i = 0; i < 3; i++) {
            // check rows
            if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != '-') {
                return true;
            }
            // check columns
            if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != '-') {
                return true;
            }
        }
        // check diagonals
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-') {
            return true;
        }
        if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != '-') {
            return true;
        }
        return false;
    }
}